(function(){
	'use strict';
	angular.module('app.client.colegiados.miembrosColegiados')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Client.colegiados.miembrosColegiados',
				config:{
					url:'/Miembros_Colegiados',
					templateUrl:'app/client/colegiados/miembrosColegiados/miembrosColegiados.html',
					controller:'MiembrosColegiados',
					controllerAs:'vm',
					title:'Miembros Habiles',
					settings:{
						nav:1,
						content:'Miembros Habiles'
					}
				}
			}
		]
	}

}());