(function(){
	'use strict';
	 angular.module('app.client.institucion.himnoCIP')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.institucion.himnoCIP',
		 		config : {
		 			url:'/Himno_CIP',
		 			templateUrl : 'app/client/institucion/himnoCIP/himnoCIP.html',
		 			controller : 'HimnoCIP',
		 			controllerAs: 'vm',		 			 
		 			title : 'Himno del Colegio de Ingenieros del Perú',
		 			settings : {
			 				nav : 3,
			 				content : 'Himno del Colegio de Ingenieros del Perú'
		 			}

		 		}
		 	}
	 	];
	 }
}());