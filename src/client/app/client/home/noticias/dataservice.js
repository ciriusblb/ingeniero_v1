(function (){
	'use strict'
	angular.module('app.client.noticias')
	.factory('noticiasService',dataService);

	function dataService ($resource,servicios,$state){
		var resource = $resource('/Noticias/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true},
			'get' : {method: 'GET'}
		})

		var service = {
			getNoticias:getNoticias
		}

		return service;

		function getNoticias(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
