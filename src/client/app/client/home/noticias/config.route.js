(function(){
	'use strict';
	 angular.module('app.client.noticias')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.noticias',
		 		config : {
		 			url:'/Noticias',
		 			templateUrl : 'app/client/home/noticias/noticias.html',
		 			controller : 'Noticias',
		 			controllerAs: 'vm',		 			 
		 			title : 'Noticias',
		 			// settings : {
			 		// 		nav : 4,
			 		// 		content : 'Noticias'
		 			// }

		 		}
		 	}
	 	];
	 }
}());