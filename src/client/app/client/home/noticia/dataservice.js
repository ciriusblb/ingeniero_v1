(function (){
	'use strict'
	angular.module('app.client.noticia')
	.factory('noticiaService',dataService);

	function dataService ($resource,servicios,$state){
		var resource = $resource('/Noticia/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true},
			'get' : {method: 'GET'}
		})

		var service = {
			getNoticia:getNoticia
		}

		return service;

		function getNoticia(){
		return resource.get($state.params).$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
