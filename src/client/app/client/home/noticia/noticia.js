(function(){
	'use strict';
	angular.module('app.client.noticia')
		.controller('Noticia',Noticia);
    Noticia.$inject = ['noticiaService'];
	function Noticia(noticiaService){
		var vm = this;
        init();
        function init(){
            noticiaService.getNoticia().then(function(data){
                vm.noticia=data;
                console.log(vm.noticia);
                // for (var i = 0; i < vm.noticias.length; i++) {
                //     vm.noticias[i].fecha = new Date(vm.noticias[i].fecha);
                // }
            })
        }
	}	
}());
