(function(){
	'use strict';
	 angular.module('app.client.noticia')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.noticia',
		 		config : {
		 			url:'/Noticia/:id',
		 			templateUrl : 'app/client/home/noticia/noticia.html',
		 			controller : 'Noticia',
		 			controllerAs: 'vm',		 			 
		 			title : 'Noticia',
		 			// settings : {
			 		// 		nav : 4,
			 		// 		content : 'Noticias'
		 			// }

		 		}
		 	}
	 	];
	 }
}());